import mymodule   #import system one
mymodule.say_hello()
print("version",mymodule.__version__)

from mymodule import __version__, say_hello # import system two
say_hello()
print( "version",__version__)

from mymodule import * # This import system import every public name but not version variable because it's start with __(double underscore)
say_hello()
# print("version", __version__) # It's does not work for starting with underscore
# I preffer first two import system


