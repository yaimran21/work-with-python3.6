def print_max(x, y):
	'''Print the maximum of two numbers.
	The two value must be integer.'''
	#convert two value must be integer if possible.
	x = int(x)
	y = int(y)
	if x > y:
		print(x, "is maximum.")
	elif x < y:
		print(y, "is maximum.")
	else:
		print("Two numbers are equal.")
print_max(10, 12)
print(print_max.__doc__)
