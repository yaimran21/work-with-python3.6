a = 50
def local_variable(a):
	print("a is:", a)
	a = 3
	print("a is changed to:", a)

var = local_variable(a)
print("It's still", a)
